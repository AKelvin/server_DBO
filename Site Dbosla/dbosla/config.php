<?php
/*These configs are neccessary in order to make Modern AAC work.*/

/*URL of website including http:// and without slash at the end! */
$config['website'] = $config['website'] = 'http://'.$_SERVER['HTTP_HOST'] . '/'.trim(dirname($_SERVER['SCRIPT_NAME']), '/.\\');

/*Database information*/
$config['database']['host'] = "127.0.0.1";
$config['database']['login'] = "root";
$config['database']['password'] = "";
$config['database']['database'] = "dbosla";

//IMPORTANT!! SET SERVER DIR HERE \/
$config['site']['server_path'] = "C:\Users\AKelvin\Documents\Kelvin\Server_s\Criando dbo\Base 1\DboSla";
 
/*Name of server*/
$config['server_name'] = "Dragon Ball Não sei que nome dá";
 
// Sistema automatico Pagseguro by Pernilonguildo
// Seu email cadastrado no pagseguro
$config['pagseguro']['email'] = 'antoniokelvin2@gmail.com';
 
// Nome do Produto
$config['pagseguro']['produtoNome'] = 'Premium points';
 
// Valor unitario do produto ou seja valor de cada ponto
// Exemplo de valores
// 100 = R$ 1,00
// 235 = R$ 2,35
// 4254 = R$ 42,54
$config['pagseguro']['produtoValor'] = '100'; 
 
// Token gerado no painel do pagseguro
$config['pagseguro']['token'] = '7CCC8CBC4DB34383B5A9790F5AD71E31';  
/*End of most important configs*/

/* Simple ticket system */
$config['newsTickerLimit'] = 4; 
$config['newsTickerWords'] = 4; 
 
/*List of cities, declare by using city ID and name eg. 2=>"Soul Server" etc.*/
$config['cities'] = array(1 =>'Small City');

/*List of vocation available to choose when creating new character*/
$config['vocations'] = array(1=>"Goku", 17=>"Vegeta", 32=>"Piccolo", 45=>"C17", 57=>"Gohan", 71=>"Trunks", 83=>"Cell", 95=>"Freeza", 111=>"Majin Boo", 127=>"Brolly", 140=>"C18", 152=>"Uub", 164=>"Goten", 178=>"Chibi Trunks", 192=>"Cooler", 206=>"Dende", 218=>"Tsuful", 230=>"Bardock", 244=>"Kuririn", 256=>"Pan", 268=>"Kaio", 280=>"Videl", 292=>"Janemba", 304=>"Tenshinhan", 316=>"Jenk", 328=>"Raditz", 340=>"C16", 352=>"Turles", 364=>"Bulma");

/*List of vocation that exists on server*/
$config['server_vocations'] = array(1=>"Goku", 2=>"Goku", 3=>"Goku", 4=>"Goku", 5=>"Goku", 6=>"Goku", 7=>"Goku", 8=>"Goku", 9=>"Goku Reborn", 10=>"Goku Reborn", 11=>"Goku Reborn", 12=>"Goku Reborn", 13=>"Goku Reborn", 14=>"Goku Reborn", 15=>"Goku Reborn", 16=>"Goku Reborn", 473=>"Goku Reborn", 17=>"Vegeta", 18=>"Vegeta", 19=>"Vegeta", 20=>"Vegeta", 21=>"Vegeta", 22=>"Vegeta", 23=>"Vegeta", 24=>"Vegeta Reborn", 25=>"Vegeta Reborn", 26=>"Vegeta Reborn", 27=>"Vegeta Reborn", 28=>"Vegeta Reborn", 29=>"Vegeta Reborn", 30=>"Vegeta Reborn", 31=>"Vegeta Reborn", 474=>"Vegeta Reborn", 32=>"Piccolo", 33=>"Piccolo", 34=>"Piccolo", 35=>"Piccolo", 36=>"Piccolo", 7=>"Piccolo Reborn", 38=>"Piccolo Reborn", 39=>"Piccolo Reborn", 40=>"Piccolo Reborn", 41=>"Piccolo Reborn", 42=>"Piccolo Reborn", 43=>"Piccolo Reborn", 44=>"Piccolo Reborn", 45=>"C17", 46=>"C17", 47=>"C17", 48=>"C17", 49=>"C17", 50=>"C17 Reborn", 51=>"C17 Reborn", 52=>"C17 Reborn", 53=>"C17 Reborn", 54=>"C17 Reborn", 55=>"C17 Reborn", 56=>"C17 Reborn", 57=>"Gohan", 58=>"Gohan", 59=>"Gohan", 60=>"Gohan", 61=>"Gohan", 62=>"Gohan", 63=>"Gohan", 64=>"Gohan Reborn", 65=>"Gohan Reborn", 66=>"Gohan Reborn", 67=>"Gohan Reborn", 68=>"Gohan Reborn", 69=>"Gohan Reborn", 70=>"Gohan Reborn", 71=>"Trunks", 72=>"Trunks", 73=>"Trunks", 74=>"Trunks", 75=>"Trunks", 76=>"Trunks Reborn", 77=>"Trunks Reborn", 78=>"Trunks Reborn", 79=>"Trunks Reborn", 80=>"Trunks Reborn", 81=>"Trunks Reborn", 82=>"Trunks Reborn", 490=>"Trunks Reborn", 83=>"Cell", 84=>"Cell", 85=>"Cell", 86=>"Cell", 87=>"Cell", 88=>"Cell Reborn", 89=>"Cell Reborn", 90=>"Cell Reborn", 91=>"Cell Reborn", 92=>"Cell Reborn", 93=>"Cell Reborn", 94=>"Cell Reborn", 95=>"Freeza", 96=>"Freeza", 97=>"Freeza", 98=>"Freeza", 99=>"Freeza", 100=>"Freeza", 101=>"Freeza", 102=>"Freeza Reborn", 103=>"Freeza Reborn", 104=>"Freeza Reborn", 105=>"Freeza Reborn", 106=>"Freeza Reborn", 107=>"Freeza Reborn", 108=>"Freeza Reborn", 109=>"Freeza Reborn", 110=>"Freeza Reborn", 111=>"Majin Boo", 112=>"Majin Boo", 113=>"Majin Boo", 114=>"Majin Boo", 115=>"Majin Boo", 116=>"Majin Boo", 117=>"Majin Boo", 118=>"Majin Boo Reborn", 119=>"Majin Boo Reborn", 120=>"Majin Boo Reborn", 121=>"Majin Boo Reborn", 122=>"Majin Boo Reborn", 123=>"Majin Boo Reborn", 124=>"Majin Boo Reborn", 125=>"Majin Boo Reborn", 126=>"Majin Boo Reborn", 475=>"Majin Boo Reborn", 127=>"Brolly", 128=>"Brolly", 129=>"Brolly", 130=>"Brolly", 131=>"Brolly", 132=>"Brolly Reborn", 133=>"Brolly Reborn", 134=>"Brolly Reborn", 135=>"Brolly Reborn", 136=>"Brolly Reborn", 137=>"Broodock [Broly] Reborn", 138=>"Brolly Reborn", 139=>"Brolly Reborn",  140=>"C18", 141=>"C18", 142=>"C18", 143=>"C18", 144=>"C18", 145=>"C18 Reborn", 146=>"C18 Reborn", 147=>"C18 Reborn", 148=>"C18 Reborn", 149=>"C18 Reborn", 150=>"C18 Reborn", 151=>"C18 Reborn", 152=>"Uub", 153=>"Uub", 154=>"Uub", 155=>"Uub", 156=>"Uub", 157=>"Uub Reborn", 158=>"Uub Reborn", 159=>"Uub Reborn", 160=>"Uub Reborn", 161=>"Uub Reborn", 162=>"Uub Reborn", 163=>"Uub Reborn", 164=>"Goten", 165=>"Goten", 166=>"Goten", 167=>"Goten", 168=>"Goten", 169=>"Goten", 170=>"Goten", 171=>"Goten Reborn", 172=>"Goten Reborn", 173=>"Goten Reborn", 174=>"Goten Reborn", 175=>"Goten Reborn", 176=>"Goten Reborn", 177=>"Goten Reborn", 178=>"Chibi Trunks", 179=>"Chibi Trunks", 180=>"Chibi Trunks", 181=>"Chibi Trunks", 182=>"Chibi Trunks", 183=>"Chibi Trunks", 184=>"Chibi Trunks", 185=>"Chibi Trunks Reborn", 186=>"Chibi Trunks Reborn", 187=>"Chibi Trunks Reborn", 188=>"Chibi Trunks Reborn", 189=>"Chibi Trunks Reborn", 190=>"Chibi Trunks Reborn", 191=>"Chibi Trunks Reborn", 192=>"Cooler", 193=>"Cooler", 194=>"Cooler", 195=>"Cooler", 196=>"Cooler", 197=>"Cooler", 198=>"Cooler Reborn", 199=>"Cooler Reborn", 200=>"Cooler Reborn", 201=>"Cooler Reborn", 202=>"Cooler Reborn", 203=>"Cooler Reborn", 204=>"Cooler Reborn", 205=>"Cooler Reborn", 493=>"Cooler Reborn", 206=>"Dende", 207=>"Dende", 208=>"Dende", 209=>"Dende", 210=>"Dende", 211=>"Dende Reborn", 212=>"Dende Reborn", 213=>"Dende Reborn", 214=>"Dende Reborn", 215=>"Dende Reborn", 216=>"Dende Reborn", 217=>"Dende Reborn", 218=>"Tsuful", 219=>"Tsuful", 220=>"Tsuful", 221=>"Tsuful", 222=>"Tsuful", 223=>"Tsuful Reborn", 224=>"Tsuful Reborn", 225=>"Tsuful Reborn", 226=>"Tsuful Reborn", 227=>"Tsuful Reborn", 228=>"Tsuful Reborn", 229=>"Tsuful Reborn", 230=>"Bardock", 231=>"Bardock", 232=>"Bardock", 233=>"Bardock", 234=>"Bardock", 235=>"Bardock", 236=>"Bardock Reborn", 237=>"Bardock Reborn", 238=>"Bardock Reborn", 239=>"Bardock Reborn", 240=>"Bardock Reborn", 241=>"Broodock [Bardock] Reborn", 242=>"Bardock Reborn", 243=>"Bardock Reborn", 244=>"Kuririn", 245=>"Kuririn", 246=>"Kuririn", 247=>"Kuririn", 248=>"Kuririn", 249=>"Kuririn Reborn", 250=>"Kuririn Reborn", 251=>"Kuririn Reborn", 252=>"Kuririn Reborn", 253=>"Kuririn Reborn", 254=>"Kuririn Reborn", 255=>"Kuririn Reborn", 256=>"Pan", 257=>"Pan", 258=>"Pan", 259=>"Pan", 260=>"Pan", 261=>"Pan Reborn", 262=>"Pan Reborn", 263=>"Pan Reborn", 264=>"Pan Reborn", 265=>"Pan Reborn", 266=>"Pan Reborn", 267=>"Pan Reborn", 268=>"Kaio", 269=>"Kaio", 270=>"Kaio", 271=>"Kaio", 272=>"Kaio", 273=>"Kaio", 274=>"Kaio Reborn", 275=>"Kaio Reborn", 276=>"Kaio Reborn", 277=>"Kaio Reborn", 278=>"Kaio Reborn", 279=>"Kaio Reborn", 280=>"Videl", 281=>"Videl", 282=>"Videl", 283=>"Videl", 284=>"Videl", 285=>"Videl Reborn", 286=>"Videl Reborn", 287=>"Videl Reborn", 288=>"Videl Reborn", 289=>"Videl Reborn", 290=>"Videl Reborn", 291=>"Videl Reborn", 292=>"Janemba", 293=>"Janemba", 294=>"Janemba", 295=>"Janemba", 296=>"Janemba", 297=>"Janemba Reborn", 298=>"Janemba Reborn", 299=>"Janemba Reborn", 300=>"Janemba Reborn", 301=>"Janemba Reborn", 302=>"Janemba Reborn", 303=>"Janemba Reborn", 491=>"Janemba Reborn", 304=>"Tenshinhan", 305=>"Tenshinhan", 306=>"Tenshinhan", 307=>"Tenshinhan", 308=>"Tenshinhan", 309=>"Tenshinhan Reborn", 310=>"Tenshinhan Reborn", 311=>"Tenshinhan Reborn", 312=>"Tenshinhan Reborn", 313=>"Tenshinhan Reborn", 314=>"Tenshinhan Reborn", 315=>"Tenshinhan Reborn", 316=>"Jenk", 317=>"Jenk", 318=>"Jenk", 319=>"Jenk", 320=>"Jenk", 321=>"Jenk Reborn", 322=>"Jenk Reborn", 323=>"Jenk Reborn", 324=>"Jenk Reborn", 325=>"Jenk Reborn", 326=>"Jenk Reborn", 327=>"Jenk Reborn", 328=>"Raditz", 329=>"Raditz", 330=>"Raditz", 331=>"Raditz", 332=>"Raditz", 333=>"Raditz Reborn", 334=>"Raditz Reborn", 335=>"Raditz Reborn", 336=>"Raditz Reborn", 337=>"Raditz Reborn", 338=>"Raditz Reborn", 339=>"Raditz Reborn", 340=>"C16", 341=>"C16", 342=>"C16", 343=>"C16", 344=>"C16", 345=>"C16 Reborn", 346=>"C16 Reborn", 347=>"C16 Reborn", 348=>"C16 Reborn", 349=>"C16 Reborn", 350=>"C16 Reborn", 351=>"C16 Reborn", 352=>"Turles", 353=>"Turles", 354=>"Turles", 355=>"Turles", 356=>"Turles", 357=>"Turles Reborn", 358=>"Turles Reborn", 359=>"Turles Reborn", 360=>"Turles Reborn", 361=>"Turles Reborn", 362=>"Turles Reborn", 363=>"Turles Reborn", 364=>"Bulma", 365=>"Bulma", 366=>"Bulma", 367=>"Bulma", 368=>"Bulma", 369=>"Bulma Reborn", 370=>"Bulma Reborn", 371=>"Bulma Reborn", 372=>"Bulma Reborn", 373=>"Bulma Reborn", 374=>"Bulma Reborn", 375=>"Bulma Reborn", 376=>"Shenron", 377=>"Shenron", 378=>"Shenron", 379=>"Shenron", 380=>"Shenron", 381=>"Shenron Reborn", 382=>"Shenron Reborn", 383=>"Shenron Reborn", 384=>"Shenron Reborn", 385=>"Shenron Reborn", 386=>"Shenron Reborn", 387=>"Shenron Reborn", 388=>"Vegetto", 389=>"Vegetto", 390=>"Vegetto", 391=>"Vegetto", 392=>"Vegetto", 393=>"Vegetto Reborn", 394=>"Vegetto Reborn", 395=>"Vegetto Reborn", 396=>"Vegetto Reborn", 397=>"Vegetto Reborn", 398=>"Vegetto Reborn", 399=>"Vegetto Reborn", 400=>"Tapion", 401=>"Tapion", 402=>"Tapion", 403=>"Tapion", 404=>"Tapion", 405=>"Tapion Reborn", 406=>"Tapion Reborn", 407=>"Tapion Reborn", 408=>"Tapion Reborn", 409=>"Tapion Reborn", 410=>"Tapion Reborn", 411=>"Tapion Reborn", 412=>"Tapion Reborn", 476=>"Tapion Reborn", 413=>"Kame", 414=>"Kame", 415=>"Kame", 416=>"Kame", 417=>"Kame", 418=>"Kame Reborn", 419=>"Kame Reborn", 420=>"Kame Reborn", 421=>"Kame Reborn", 422=>"Kame Reborn", 423=>"Kame Reborn", 424=>"Kame Reborn", 425=>"King", 426=>"King", 427=>"King", 428=>"King", 429=>"King", 430=>"King Reborn", 431=>"King Reborn", 432=>"King Reborn", 433=>"King Reborn", 434=>"King Reborn", 435=>"King Reborn", 436=>"King Reborn", 437=>"Kagome", 438=>"Kagome", 439=>"Kagome", 440=>"Kagome", 441=>"Kagome", 442=>"Kagome Reborn", 443=>"Kagome Reborn", 444=>"Kagome Reborn", 445=>"Kagome Reborn", 446=>"Kagome Reborn", 447=>"Kagome Reborn", 448=>"Kagome Reborn", 449=>"Zaiko", 450=>"Zaiko", 451=>"Zaiko", 452=>"Zaiko", 453=>"Zaiko", 454=>"Zaiko Reborn", 455=>"Zaiko Reborn", 456=>"Zaiko Reborn", 457=>"Zaiko Reborn", 458=>"Zaiko Reborn", 459=>"Zaiko Reborn", 460=>"Zaiko Reborn", 477=>"Zaiko Reborn", 461=>"Lord Chilled", 462=>"Lord Chilled", 463=>"Lord Chilled", 464=>"Lord Chilled", 465=>"Lord Chilled", 466=>"Lord Chilled Reborn", 467=>"Lord Chilled Reborn", 468=>"Lord Chilled Reborn", 469=>"Lord Chilled Reborn", 470=>"Lord Chilled Reborn", 471=>"Lord Chilled Reborn", 472=>"Lord Chilled Reborn", 492=>"Lord Chilled Reborn", 478=>"C8", 479=>"C8", 480=>"C8", 481=>"C8", 482=>"C8", 483=>"C8 Reborn", 484=>"C8 Reborn", 485=>"C8 Reborn", 486=>"C8 Reborn", 487=>"C8 Reborn", 488=>"C8 Reborn", );


// Fiz até o 488 do C8, dia(24/07/2018), falta mais algumas..

/*List of promotions, the key is vocation without promotion*/
$config['promotions'] = array(1=>"Master Sorcerer", 2=>"Elder Druid", 3=>"Royal Paladin", 4=>"Elite Knight");
 
/*Resitricted names*/
$config['restricted_names'] = array("god", "gamemaster", "admin", "account manager", "owner");
 
/*Names with any of this value cannot be created*/
$config['invalidNameTags'] = array("god", "gm", "cm", "gamemaster", "hoster", "admin", "owner");
 
 
/*ID and names of worlds*/
$config['worlds'][0] = "Terra";
 
// Enable multiworld by uncommenting this
//$config['worlds'][1] = "Second World";
 
/* Addresses of each server */
$config['servers'][0] = array('address'=>'127.0.0.1', 'port'=>7171, 'vapusid'=>'%VAPUS_ID%');
 
// Enable multiworld by uncommenting this
//$config['servers'][1] = array('address'=>'127.0.0.1', 'port'=>7173, 'vapusid' => 'XXX');
 
/*Groups that exists on server*/
$config['groups'] = array(0=>"Player", 1=>"Player", 2=>"Tutor", 3=>"Senior Tutor", 4=>"Gamemaster", 5=>"Community Manager", 6=>"administrador", 7=>"Owner");
 
 
/*Names of vocations as in database as samples. First key is world id and second vocation id.*/
$config['newchar_vocations'][0][1] = "Goku Sample";
$config['newchar_vocations'][0][17] = "Vegeta Sample";
$config['newchar_vocations'][0][32] = "Piccolo Sample";
$config['newchar_vocations'][0][45] = "C17 Sample";
$config['newchar_vocations'][0][57] = "Gohan Sample";
$config['newchar_vocations'][0][71] = "Trunks Sample";
$config['newchar_vocations'][0][83] = "Cell Sample";
$config['newchar_vocations'][0][95] = "Freeza Sample";
$config['newchar_vocations'][0][111] = "Majin Boo Sample";
$config['newchar_vocations'][0][127] = "Brolly Sample";
$config['newchar_vocations'][0][140] = "C18 Sample";
$config['newchar_vocations'][0][164] = "Goten Sample";
$config['newchar_vocations'][0][178] = "Chibi Trunks Sample";
$config['newchar_vocations'][0][192] = "Cooler Sample";
$config['newchar_vocations'][0][206] = "Dende Sample";
$config['newchar_vocations'][0][218] = "Tsuful Sample";
$config['newchar_vocations'][0][230] = "Bardock Sample";
$config['newchar_vocations'][0][244] = "Kuririn Sample";
$config['newchar_vocations'][0][256] = "Pan Sample";
$config['newchar_vocations'][0][268] = "Kaio Sample";
$config['newchar_vocations'][0][280] = "Videl Sample";
$config['newchar_vocations'][0][292] = "Janemba Sample";
$config['newchar_vocations'][0][304] = "Tenshinhan Sample";
$config['newchar_vocations'][0][316] = "Jenk Sample";
$config['newchar_vocations'][0][328] = "Raditz Sample";
$config['newchar_vocations'][0][340] = "C16 Sample";
$config['newchar_vocations'][0][352] = "Turles Sample";
$config['newchar_vocations'][0][364] = "Bulma Sample";

/*Don't show chaarcters with group id higher than*/
$config['players_group_id_block'] = 3;


/*Min. level to create guild*/
$config['levelToCreateGuild'] = 150;


/*Limit of latest deaths*/
$config['latestdeathlimit'] = 20;

/*Limit news per page*/
$config['newsLimit'] = 100;

/*Limit comments per page*/
$config['commentLimit'] = 0;

/*Template that should be used on website*/
$config['layout'] = "naruto";

/*Title of a website*/
$config['title'] = "DBO Léo é Gay";


/*Premdays given when creating new account.*/
$config['premDays'] = 0;


/*Positions to start when creating character*/
$startPos['x'] = 655;
$startPos['y'] = 399;
$startPos['z'] = 7;


/*Trigger password for scaffolding system.*/
$config['scaffolding_trigger'] = "password";

/*Minimum page access for admin priviliges*/
$config['adminAccess'] = 6;

/*Max threads per page*/
$config['threadsLimit'] = 10;

/*Max posts per page in a thread*/
$config['postsLimit'] = 0;

/*Time between posts*/
$config['timeBetweenPosts'] = 300;

/*Limit of submissions per page in bug tracker*/
$config['bugtrackerPageLimit'] = 10;

/*Limit of houses on listing page*/
$config['housesLimit'] = 10;

/*Level to buy house*/
$config['houseLevel'] = 120;

/*Lenght of housing auction in seconds*/
$config['houseAuctionTime'] = 604800;

/*Default timezone*/
$config['timezone'] = "Europe/London";

/*Allowed IPs to use command prompt in admin panel*/
$config['allowedToUseCMD'] = array("127.0.0.1", "localhost");

/* Path to your UI theme */
$config['UItheme'] = "smoothness/jquery-ui-1.7.2.custom.css";

/*Destination to guilds logos folder, must be writable.*/
$config['uploads'] = "/public/guild_logos/";

/* Status timeout (recheck if server is online) */
$config['statusTimeout'] = 1 + (5 * 60); // Default to 5min

/* Wrap words longer than */
$config['wrap_words'] = 80;

/*Limit comments per page in videos view*/
$config['videoCommentsLimit'] = 0;

/*Limit of videos to show while searching*/
$config['videoSearchLimit'] = 0;

/*Maximum amount of characters per account*/
$config['maxCharacters'] = 7;

/*Limit of inbox/outbox messages per page*/
$config['messagesLimit'] = 0;

/*Amount of names to be saved when looking for characters*/
$config['characterSearchLimit'] = 10;

/*Switch on Admin Window*/
$config['adminWindow'] = true;

/*Integrate facebook to AAC? (TRUE/FALSE)*/
$config['facebook'] = false;

/*Max amount of saved actions*/
$config['actionsCount'] = 15;

/*Player per page in hishscore */
$config['highscore']['per_page'] = 15;

/*Total players to show in highscores*/
$config['highscore']['total'] = 100;


/* Guild board creation */
$config['guildboardTitle'] = "Guildboard for %NAME%";
$config['guildboardDescription'] = "This is the guildboard for the great %NAME% guild!";

/* VAPus Settings */
$config['VAPusGraphStep'] = 1; // step * update time = time steps on graph, etc 6 with an update time of 10min = one hour



//Enable delay between creating characters
$config['characterDelay'] = true;

//Time between creating characters in seconds
$config['characterDelayTime'] = 60;

//Enable delay between creating accounts
$config['accountDelay'] = true;

//Time between creating accounts in seconds
$config['accountDelayTime'] = 240;

//Account restrictions
$config['restrictedAccounts'] = array('1'); 

############EVENTS############

# Event fired just after main framework to gain access to all features
$config['onLoad'] = array();

# Event fired after all finished loading no headers should be sent
$config['onReady'] = array();


#############################

/*
######################################################################################################################
 * Do not touch any of the configs below if you are not 100% sure what you are doing!
 * These are config to the engine, usually the default ones works well so no change needed for unexperienced users.
######################################################################################################################
*/
// Tiny hack to figure if we use Windows or not.
if (strtoupper(substr(PHP_OS, 0, 3)) == 'WIN') @define('USING_WINDOWS', 1);
else @define('USING_WINDOWS', 0);

if(USING_WINDOWS) $config['engine']['PHPversion'] = "5.3.0";
else $config['engine']['PHPversion'] = "5.3.0";
$config['engine']['indexPage'] = "index.php";
$config['engine']['uri_protocol'] = "AUTO";
$config['engine']['charSET'] = "UTF-8";
$config['engine']['enable_hooks'] = FALSE;
$config['engine']['permitted_uri_chars'] = "a-z 0-9~%.:_\-'+";
$config['engine']['enable_query_strings'] = FALSE;
$config['engine']['global_xss_filtering'] = TRUE;
$config['engine']['compress_output'] = FALSE;
$config['engine']['proxy_ip'] = "";
$config['engine']['autoload_libraries'] = array();
$config['engine']['autoload_helper'] = array();
$config['engine']['autoload_plugin'] = array();
$config['engine']['autoload_config'] = array();
$config['engine']['autoload_model'] = array();
$config['engine']['default_controller'] = "home";
$config['engine']['platforms'] = array('windows nt 6.0' => 'Windows Longhorn', 'windows nt 5.2' => 'Windows 2003', 'windows nt 5.0' => 'Windows 2000', 'windows nt 5.1' => 'Windows XP', 'windows nt 4.0' => 'Windows NT 4.0', 'winnt4.0' => 'Windows NT 4.0', 'winnt 4.0' => 'Windows NT', 'winnt' => 'Windows NT', 'windows 98' => 'Windows 98', 'win98' => 'Windows 98', 'windows 95' => 'Windows 95', 'win95' => 'Windows 95', 'windows' => 'Unknown Windows OS', 'os x' => 'Mac OS X', 'ppc mac' => 'Power PC Mac', 'freebsd' => 'FreeBSD', 'ppc' => 'Macintosh', 'linux' => 'Linux', 'debian' => 'Debian', 'sunos' => 'Sun Solaris', 'beos' => 'BeOS', 'apachebench' => 'ApacheBench', 'aix' => 'AIX', 'irix' => 'Irix', 'osf' => 'DEC OSF', 'hp-ux' => 'HP-UX', 'netbsd' => 'NetBSD', 'bsdi' => 'BSDi', 'openbsd' => 'OpenBSD', 'gnu' => 'GNU/Linux', 'unix' => 'Unknown Unix OS' );
$config['engine']['mobiles'] = array('mobileexplorer' => 'Mobile Explorer', 'palmsource' => 'Palm', 'palmscape' => 'Palmscape', 'motorola' => "Motorola", 'nokia' => "Nokia", 'palm' => "Palm", 'iphone' => "Apple iPhone", 'ipod' => "Apple iPod Touch", 'sony' => "Sony Ericsson", 'ericsson' => "Sony Ericsson", 'blackberry' => "BlackBerry", 'cocoon' => "O2 Cocoon", 'blazer' => "Treo", 'lg' => "LG", 'amoi' => "Amoi", 'xda' => "XDA", 'mda' => "MDA", 'vario' => "Vario", 'htc' => "HTC", 'samsung' => "Samsung", 'sharp' => "Sharp", 'sie-' => "Siemens", 'alcatel' => "Alcatel", 'benq' => "BenQ", 'ipaq' => "HP iPaq", 'mot-' => "Motorola", 'playstation portable' => "PlayStation Portable", 'hiptop' => "Danger Hiptop", 'nec-' => "NEC", 'panasonic' => "Panasonic", 'philips' => "Philips", 'sagem' => "Sagem", 'sanyo' => "Sanyo", 'spv' => "SPV", 'zte' => "ZTE", 'sendo' => "Sendo", 'symbian' => "Symbian", 'SymbianOS' => "SymbianOS", 'elaine' => "Palm", 'palm' => "Palm", 'series60' => "Symbian S60", 'windows ce' => "Windows CE", 'obigo' => "Obigo", 'netfront' => "Netfront Browser", 'openwave' => "Openwave Browser", 'mobilexplorer' => "Mobile Explorer", 'operamini' => "Opera Mini", 'opera mini' => "Opera Mini", 'digital paths' => "Digital Paths", 'avantgo' => "AvantGo", 'xiino' => "Xiino", 'novarra' => "Novarra Transcoder", 'vodafone' => "Vodafone", 'docomo' => "NTT DoCoMo", 'o2' => "O2", 'mobile' => "Generic Mobile", 'wireless' => "Generic Mobile", 'j2me' => "Generic Mobile", 'midp' => "Generic Mobile", 'cldc' => "Generic Mobile", 'up.link' => "Generic Mobile", 'up.browser' => "Generic Mobile", 'smartphone' => "Generic Mobile", 'cellphone' => "Generic Mobile" );
$config['engine']['robots'] = array('googlebot' => 'Googlebot', 'msnbot' => 'MSNBot', 'slurp' => 'Inktomi Slurp', 'yahoo' => 'Yahoo', 'askjeeves' => 'AskJeeves', 'fastcrawler' => 'FastCrawler', 'infoseek' => 'InfoSeek Robot 1.0', 'lycos' => 'Lycos' );
$config['engine']['browsers'] = array('Opera' => 'Opera', 'MSIE' => 'Internet Explorer', 'Internet Explorer' => 'Internet Explorer', 'Shiira' => 'Shiira', 'Firefox' => 'Firefox', 'Chimera' => 'Chimera', 'Phoenix' => 'Phoenix', 'Firebird' => 'Firebird', 'Camino' => 'Camino', 'Netscape' => 'Netscape', 'OmniWeb' => 'OmniWeb', 'Safari' => 'Safari', 'Mozilla' => 'Mozilla', 'Konqueror' => 'Konqueror', 'icab' => 'iCab', 'Lynx' => 'Lynx', 'Links' => 'Links', 'hotjava' => 'HotJava', 'amaya' => 'Amaya', 'IBrowse' => 'IBrowse' );
$config['engine']['mimes'] = array('hqx' => 'application/mac-binhex40', 'cpt' => 'application/mac-compactpro', 'csv' => array ('text/x-comma-separated-values', 'text/comma-separated-values', 'application/octet-stream', 'application/vnd.ms-excel', 'text/csv', 'application/csv', 'application/excel', 'application/vnd.msexcel' ), 'bin' => 'application/macbinary', 'dms' => 'application/octet-stream', 'lha' => 'application/octet-stream', 'lzh' => 'application/octet-stream', 'exe' => 'application/octet-stream', 'class' => 'application/octet-stream', 'psd' => 'application/x-photoshop', 'so' => 'application/octet-stream', 'sea' => 'application/octet-stream', 'dll' => 'application/octet-stream', 'oda' => 'application/oda', 'pdf' => array ('application/pdf', 'application/x-download' ), 'ai' => 'application/postscript', 'eps' => 'application/postscript', 'ps' => 'application/postscript', 'smi' => 'application/smil', 'smil' => 'application/smil', 'mif' => 'application/vnd.mif', 'xls' => array ('application/excel', 'application/vnd.ms-excel', 'application/msexcel' ), 'ppt' => array ('application/powerpoint', 'application/vnd.ms-powerpoint' ), 'wbxml' => 'application/wbxml', 'wmlc' => 'application/wmlc', 'dcr' => 'application/x-director', 'dir' => 'application/x-director', 'dxr' => 'application/x-director', 'dvi' => 'application/x-dvi', 'gtar' => 'application/x-gtar', 'gz' => 'application/x-gzip', 'php' => 'application/x-httpd-php', 'php4' => 'application/x-httpd-php', 'php3' => 'application/x-httpd-php', 'phtml' => 'application/x-httpd-php', 'phps' => 'application/x-httpd-php-source', 'js' => 'application/x-javascript', 'swf' => 'application/x-shockwave-flash', 'sit' => 'application/x-stuffit', 'tar' => 'application/x-tar', 'tgz' => 'application/x-tar', 'xhtml' => 'application/xhtml+xml', 'xht' => 'application/xhtml+xml', 'zip' => array ('application/x-zip', 'application/zip', 'application/x-zip-compressed' ), 'mid' => 'audio/midi', 'midi' => 'audio/midi', 'mpga' => 'audio/mpeg', 'mp2' => 'audio/mpeg', 'mp3' => array ('audio/mpeg', 'audio/mpg' ), 'aif' => 'audio/x-aiff', 'aiff' => 'audio/x-aiff', 'aifc' => 'audio/x-aiff', 'ram' => 'audio/x-pn-realaudio', 'rm' => 'audio/x-pn-realaudio', 'rpm' => 'audio/x-pn-realaudio-plugin', 'ra' => 'audio/x-realaudio', 'rv' => 'video/vnd.rn-realvideo', 'wav' => 'audio/x-wav', 'bmp' => 'image/bmp', 'gif' => 'image/gif', 'jpeg' => array ('image/jpeg', 'image/pjpeg' ), 'jpg' => array ('image/jpeg', 'image/pjpeg' ), 'jpe' => array ('image/jpeg', 'image/pjpeg' ), 'png' => array ('image/png', 'image/x-png' ), 'tiff' => 'image/tiff', 'tif' => 'image/tiff', 'css' => 'text/css', 'html' => 'text/html', 'htm' => 'text/html', 'shtml' => 'text/html', 'txt' => 'text/plain', 'text' => 'text/plain', 'log' => array ('text/plain', 'text/x-log' ), 'rtx' => 'text/richtext', 'rtf' => 'text/rtf', 'xml' => 'text/xml', 'xsl' => 'text/xml', 'mpeg' => 'video/mpeg', 'mpg' => 'video/mpeg', 'mpe' => 'video/mpeg', 'qt' => 'video/quicktime', 'mov' => 'video/quicktime', 'avi' => 'video/x-msvideo', 'movie' => 'video/x-sgi-movie', 'doc' => 'application/msword', 'docx' => 'application/vnd.openxmlformats-officedocument.wordprocessingml.document', 'xlsx' => 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet', 'word' => array ('application/msword', 'application/octet-stream' ), 'xl' => 'application/excel', 'eml' => 'message/rfc822' );
$config['engine']['doctypes'] = array('xhtml11' => '<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.1//EN" "http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd">', 'xhtml1-strict' => '<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">', 'xhtml1-trans' => '<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">', 'xhtml1-frame' => '<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Frameset//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-frameset.dtd">', 'html5' => '<!DOCTYPE html>', 'html4-strict' => '<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01//EN" "http://www.w3.org/TR/html4/strict.dtd">', 'html4-trans' => '<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">', 'html4-frame' => '<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Frameset//EN" "http://www.w3.org/TR/html4/frameset.dtd">' );
$config['engine']['url_suffix'] = ".ide";
$config['engine']['sess_cookie_name'] = 'ci_session';
$config['engine']['sess_expiration'] = 7200;
$config['engine']['sess_encrypt_cookie'] = FALSE;
$config['engine']['sess_use_database'] = FALSE;
$config['engine']['sess_table_name'] = 'ci_sessions';
$config['engine']['sess_match_ip'] = FALSE;
$config['engine']['sess_match_useragent'] = TRUE;
$config['engine']['sess_time_to_update'] = 300;
$config['engine']['rewrite_short_tags'] = false;
if(USING_WINDOWS == 1) {
//Load management is not available on Windows.
$config['engine']['loadManagement'] = false;
} else {
//Load management is a maximum ammount of processes website is using. If the website is flooded it will drop connection with users outside this amount.
$config['engine']['loadManagement'] = false;
$config['engine']['maxLoad'] = 60;
}

/*
|--------------------------------------------------------------------------
| Error Logging Threshold
|--------------------------------------------------------------------------
|
| If you have enabled error logging, you can set an error threshold to 
| determine what gets logged. Threshold options are:
| You can enable error logging by setting a threshold over zero. The
| threshold determines what gets logged. Threshold options are:
|
|	0 = Disables logging, Error logging TURNED OFF
|	1 = Error Messages (including PHP errors)
|	2 = Debug Messages
|	3 = Informational Messages
|	4 = All Messages
|
| For a live site you'll usually only enable Errors (1) to be logged otherwise
| your log files will fill up very fast.
|
*/
$config['engine']['log_threshold'] = 0;


#DON'T TOUCH! DECLARING CONSTANS!
@DEFINE('LEVELTOCREATEGUILD', $config['levelToCreateGuild']);
@DEFINE('PREMDAYS', $config['premDays']);
@DEFINE('HOSTNAME', $config['database']['host']);
@DEFINE('USERNAME', $config['database']['login']);
@DEFINE('PASSWORD', $config['database']['password']);
@DEFINE('DATABASE', $config['database']['database']);
@DEFINE('WEBSITE', $config['website']);
?>